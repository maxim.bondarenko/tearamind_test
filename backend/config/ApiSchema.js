const FileApi = require("../services/staticFiles/entryPoints/Api");
const UsersApi = require("../services/users/entryPoints/Api");
const LoginApi = require("../services/login/entryPoints/Api");

const __callMethod = (req, res, serviceMethod) => {
    try{
         serviceMethod(req,res);
    }
    catch(exception)
    {
        console.log(exception.message);
        return res.status(500).send({ message: "something went wrong" })
    }
};

module.exports = {
    "post": {
        "/users/": (req, res) => { __callMethod(req, res, new UsersApi().createUser)}
    },
    "post": {
        "/users/:uid/files/:full_file_path*":  (req, res) => { __callMethod(req, res, new FileApi().getFiles)}
    },
    "get": {
        "/static/files/:full_file_path*":  (req, res) => { __callMethod(req, res, new FileApi().getFiles)}
    }
};