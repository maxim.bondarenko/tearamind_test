module.exports = {
    db:"mongodb", //this is static data for connection using in mongoose library
    host:process.env.BACKEND_DB_HOST,
    port: process.env.BACKEND_DB_PORT,
    dbName:process.env.BACKEND_DB_NAME
};
