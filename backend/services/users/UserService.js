
const UserModel = require("./models/UserModel");

const EntityDuplicateKeyException = require("./exceptions/EntityDuplicateKeyException");
const InternalError = require("./exceptions/InternalError");
const NotFoundEntityException = require("./exceptions/NotFoundEntityException");
class UserService
{
    async createUser(user)
    {
        let userData = await this.__getModel().create(user);
        userData = userData && userData.toObject ? userData.toObject() : null;
        userData.cid = user.cid;
        return userData;
    }

    getUser(email, password)
    {
        return new Promise((resolve, reject) => {
            let pr;
            if(password)
            {
                pr = this.__getModel().getUserByEmailPass(email,password);
            }
            else{
                pr = this.__getModel().getUserByEmail(email);
            }
            pr.then((user, err) => {
                if(err)
                {
                    return this.__errorProcessing(err, reject);
                }
                if(!user)
                {
                    return;
                }

                return resolve(user.toObject());
            }).catch(err => { 
                console.log('getUser', err); 
                this.__errorProcessing(err, reject); 
            });
        });
    }

    getUserById(id)
    {
        return new Promise((resolve, reject) => {
            this.__getModel().getUserById(id).then((user, err) => {
                if(err)
                {
                    return this.__errorProcessing(err, reject);
                }
                if(!user)
                {
                    return reject(new NotFoundEntityException("Entity not found"));
                }

                return resolve(user.toObject());
            }).catch(err => { this.__errorProcessing(err, reject); });
        });
    }

    __getModel()
    {
        return new UserModel();
    }

    __errorProcessing(err, reject)
    {
        console.log('__errorProcessing', err);
        let errmsg = err ? err.errmsg || '' : '';
        if(err && err.code === 11000)
        {
            return reject( new EntityDuplicateKeyException(errmsg));
        }
        reject( new InternalError(errmsg) );
    }
}

module.exports = new UserService();
