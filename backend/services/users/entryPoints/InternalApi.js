const userService = require("../UserService");

class InternalApi
{
    /**
     * @param {String} id Unique user id
     *
     * @return {Object} User Method returns User object data.
     */
    getUserById(id)
    {
        return userService.getUserById(id);
    }

    /**
     * @param {String} email Unique user email
     *
     * @return {Object} User Method returns User object data.
     */
    getUserByEmail(email)
    {
        return userService.getUser(email);
    }
    /**
     * @param {Object} user data for created
     *
     * @return {Object} User Method returns created User object data.
     */
    create(userData)
    {
        return userService.createUser(userData);
    }


    /**
     * @param {String} email Unique user email
     * @param {String} password User password
     *
     * @return {Object} User Method returns User object data.
     */
    getUserByEmailAndPass(email, password)
    {
        return userService.getUser(email, password);
    }
}

module.exports = new InternalApi();