const userService = require("../UserService");
const ApiMapper = require("../mappers/ApiMapper");

class Api
{
    /**
     * Returns user by uid from token or 404 if user not exist
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {String} req.params.email  User email.
     *
     */
    async getMyData(req, res)
    {
        if(!req.user || !req.user.uid)
        {
            res.status(401).send(
                {code:'getUserData.0.1', message: "User are not authorized"}
            );
        }
        let user = await userService.getUserById(req.user.uid);
        if(!user)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            user
        );
    }
    /**
     * Create new user and returns created user data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async createUser(req, res)
    {
        let mappedObject = ApiMapper.toServerProperty(req.body);


        let user = await userService.getUser(mappedObject.email);

        if(user)
        {
            return res.status(400).send(
                { message: "Use already exist"}
            );
        }

        user = await userService.createUser(mappedObject);
        if(!user)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        res.status(200).send(
            user
        );
    }
}

module.exports = Api;
