const Mapper = require("../../../libs/Mapper");

class ApiMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "_id",
                toClient: "uid",
                toServerSkipped: true
            },
            {
                toServer: "email",
                toClient: "email",
                type: "string",
            },
            {
                toServer: "password",
                toClient: "password",
                toClienSkiped: true,
                type: "string",
            }
        ];
    }
}

module.exports = new ApiMapper();