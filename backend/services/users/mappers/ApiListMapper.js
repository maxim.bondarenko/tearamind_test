const Mapper = require("../../../libs/Mapper");
const systemRoles =  require('../../../config/system_roles');
const ValidationError = require("../exceptions/ValidationError");

class ApiMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "_id",
                toClient: "uid",
                toServerSkipped: true
            },
            {
                toServer: "comp",
				toClient: "cid",
				type: "string",
            },
            {
                toServer: "email",
                toClient: "email",
                type: "string",
            },
            {
                toServer: "isActive",
                toClient: "isActive",
                type: "boolean"
            },
            {
                toServer: "password",
                toClient: "password",
                toClienSkiped: true,
                type: "string",
            },
            {
                toServer: "birthday",
                toClient: "birthday",
                type: "string"
            },
            {
                toServer: "phone",
                toClient: "phone",
                type: "string"
            },
            {
                toServer: "name",
                toClient: "name",
                type: "string",
            },
            {
                toServer: "role",
                toClient: "role",
                type: "string",
                toServerFunc: (role => {
                    if(Object.values(systemRoles).indexOf(role) === -1)
                    {
                        throw new ValidationError("value for role are not allowed")
                    }
                    return role;
                }),
            }
        ];
    }
}

module.exports = new ApiMapper();