const mongoose = require('../../../libs/mongoose').mongoose;
const bcrypt = require('bcryptjs');
const bcryptConf = require('../config/bcrypt_conf');

// define the User model schema
const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        index: true,
        unique: true
    },
    password: String,

    createdDate:{ type: Date, default: Date.now},
});


UserSchema.methods.comparePassword = function comparePassword(password) {
    return bcrypt.compareSync(password, this.password);
};


/**
 * The pre-save hook method.
 */

UserSchema.pre('save', function saveHook(next) {
    const user = this;
    // proceed further only if the password is modified or the user is new
    if (!user.isModified('password')) return next();


    return bcrypt.genSalt(bcryptConf.saltCount, (saltError, salt) => {
        if (saltError) { return next(saltError); }

        return bcrypt.hash(user.password, bcryptConf.saltCount, (hashError, hash) => {
            if (hashError) { return next(hashError); }

            // replace a password string with hash value
            user.password = hash;
            return next();
        });
    });
});

const provider = mongoose.model('users', UserSchema);

class UserModel
{

	__getLookup()
    {
        return null;
    }

    getProvider()
    {
        return provider;
    }

    create(user)
    {
        //TODO::validation

        let userData = {
            email: user.email,
            password: user.password
        };
        return provider.create(userData);
    }

    getAllUsers(byRoles)
    {
        return provider.find(byRoles).lean().exec();
    }

    getUserByEmailPass(email, password)
    {
        return new Promise((resolve, reject) => {
            this.getUserByEmail(email).then((user, err) => {
                if(err) return reject(err);
                if (user && !user.comparePassword(password))
                {
                    return reject("Incorrect password");
                }
                resolve(user);
            }).catch((err) => reject(err));
        });
    }

    getUserByUidPass(uid, password)
    {
        return new Promise((resolve, reject) => {
            this.getUserById(uid).then((user, err) => {
                if(err) return reject(err);
                if (user && !user.comparePassword(password))
                {
                    return reject("Incorrect password");
                }
                resolve(user);
            }).catch((err) => reject(err));
        });
    }

    removeUser(email)
    {
        return provider.find({email: email}).remove().exec();
    }

    getUserByEmail(email)
    {
        return provider.findOne({email: email}).populate('comp').exec();
    }

    updateUserByUid(userData, uid)
    {
        return provider.findOneAndUpdate({_id: mongoose.Types.ObjectId(uid)}, {$set: userData}, {new: true}).exec();
    }

    async setNewPass(uid, newPass)
    {
        let user = await provider.findOne({_id: mongoose.Types.ObjectId(uid)}).exec();
        user.set("password", newPass);
        return user.save();
    }

    async resetPass(uid, newPass, oldPass)
    {
        let user = await this.getUserByUidPass(uid, oldPass);
        user.password = newPass;
        return user.save();
    }

    getUserById(uid)
    {
        return provider.findOne({_id: mongoose.Types.ObjectId(uid)}).populate('comp').exec();
    }
}

module.exports = UserModel;
