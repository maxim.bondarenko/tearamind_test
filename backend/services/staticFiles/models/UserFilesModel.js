const mongoose = require('../../../libs/mongoose').mongoose;

// define the User model schema
const UserFiles = new mongoose.Schema({
    uid:{ type: mongoose.Schema.Types.ObjectId, index: true }, //user_id
    filePath: String,
    createdDate:{ type: Date, default: Date.now},
});

const provider = mongoose.model('users_files', UserFiles);

class UserFilesModel
{

	__getLookup()
    {
        return null;
    }

    getProvider()
    {
        return provider;
    }

    create(uid, filePath)
    {
        //TODO::validation

        let userData = {
            uid: uid,
            filePath: filePath
        };
        return provider.create(userData);
    }

    getAllFilesByUser(uid)
    {
        return provider.find({uid: mongoose.Types.ObjectId(uid)}).lean().exec();
    }
}

module.exports = UserFilesModel;
