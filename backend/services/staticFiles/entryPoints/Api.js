const staticFilesService = require("../staticFilesService");
const FileStorage = require("../../../libs/fileStorage");
const express = require('express');
class Api
{

    getSchema()
    {
        return {
            "get": {
                "/static/files/:full_file_path*": {
                    act: this.getFiles
                }
            }
        };
    }

    getFiles(req, res, next)
    {
        req.url = req.url.replace('/static/files/uploads', '');
        express.static('uploads')(req,res,next);
    }

    async uploadFile(req, res)
    {
        let users = await  userService.getUserById(req.user.uid);

        if(!users)
        {
            return res.status(500).send(
                { message: "Something went wrong"}
            );
        }
        this.uid = users._id;
        let fail ='';
        let fileStorage = new FileStorage(`${users._id}`, true);

        let upload = fileStorage.getUpload(
        //     (req, file, cb)  => {
        //     let filetypes = /jpeg|jpg|png|gif/;
        //     let mimetype = filetypes.test(file.mimetype);
        //     //
        //
        //     if (mimetype) {
        //         return cb(null, true);
        //     }
        //     cb("Error: File upload only supports the following filetypes - " + filetypes);
        // }
        ).any();

        upload(req, res, (err) => {

            if (err) {
                return res.status(500).send(
                    { message: "Something went wrong"}
                );
            }

            if(!req.files || !req.files[0])
            {
                //то отправляем ошибку 400
                return res.status(400).send(
                    { message: "No files fonded"}
                );
            }

            let file = req.files[0].path;

            return staticFilesService.createUserFile(file, req.params.uid).then(successData => {
                return res.status(200).send(
                    successData
                );
            }).catch(err => {
                return res.status(500).send(
                    { message: "Something went wrong"}
                );
            });
        });
    }
}

module.exports = Api;