const serviceContainer = require("../../libs/ServiceContainer");
const UsersFilesModel = require("./models/UserFilesModel")


class StaticFilesService
{
    async createUserFile(filePath, uid)
    {
        let fileData = await this.__getModel().create(uid, filePath);
        return fileData;
    }

    getFilesByUid(id)
    {
        return new Promise((resolve, reject) => {
            this.__getModel().getAllFilesByUser(id).then((files, err) => {
                if(err)
                {
                    return this.__errorProcessing(err, reject);
                }
                if(!files)
                {
                    return reject("Entity not found");
                }

                return resolve(files);
            }).catch(err => { this.__errorProcessing(err, reject); });
        });
    }

    __getModel()
    {
        return new UsersFilesModel();
    }

    __errorProcessing(err, reject)
    {
        console.log('__errorProcessing', err);
        let errmsg = err ? err.errmsg || '' : '';
        if(err && err.code === 11000)
        {
            return reject( nerrmsg);
        }
        reject( errmsg );
    }
}


module.exports = new StaticFilesService();