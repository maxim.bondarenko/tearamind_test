const Mapper = require("../../../libs/Mapper");

class RegistrationApiMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "email",
                toClient: "email",
                type: "string",
                required: true
            },
            {
                toServer: "password",
                toClient: "password",
                type: "string",
                required: true
            }
        ];
    }
}

module.exports = RegistrationApiMapper;