const Mapper = require("../../../libs/Mapper");

class ApiMapper extends Mapper
{
    getPropertyMapping()
    {
        return [
            {
                toServer: "title",
                toClient: "name",
                type: "string",
            },
            {
                toServer: "isActive",
                toClient: "isActive",
                type: "boolean"
            },
            {
                toServer: "description",
                toClient: "description",
                type: "string",
            },
            {
                toServer: "createdDate",
                toClient: "createdDate",
                toServerSkipped: true,
                type: "string",
            }
        ];
    }
}

module.exports = ApiMapper;