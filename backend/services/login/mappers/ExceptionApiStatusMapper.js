
class ExceptionApiStatusMapper {
    constructor() {
        this.mapStatus = {
            "ValidationError": 400,
            "NotFoundEntityException": 404,
            "Error": 500,
            "InternalError": 500,
            "EntityDuplicateKeyException": 400
        };

        this.mapMessage = {
            "InternalError": "Something was wrong",
            "NotFoundEntityException": "Entity doesn't exists",
            "EntityDuplicateKeyException": "Entity already exists"
        }
    }

    getApiStatus(exception) {
        if (this.mapStatus[exception.constructor.name]) {
            return this.mapStatus[exception.constructor.name];
        }
        return 500;
    }

    getApiStatusMessage(exception) {
        if (this.mapMessage[exception.constructor.name]) {
            return this.mapMessage[exception.constructor.name];
        }
        return exception.message;
    }
}

module.exports = ExceptionApiStatusMapper;