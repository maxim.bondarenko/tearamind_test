const LoginService = require("../LoginService");

const exceptionMapper = require("../mappers/ExceptionApiStatusMapper");
const RegistrationApiMapper = require("../mappers/RegistrationApiMapper");

const serviceContainer = require("../../../libs/ServiceContainer");

const path = require('path');

class Api
{
    /**
     * Login by strategy and return token with the data or set error to response
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async login(req, res)
    {
        //let mappedObject = new ApiMapper().toServerProperty(req.body);
        if(!req.body.service)
        {
            return res.status(400).send(
                {code: "login.1", message:"Required field *service* is missing"}
            );
        }


        try{
            let item = await new LoginService().login(req.params.method, req.body);
            if(!item)
            {
                return res.status(400).send(
                    {code: "login.0.1", message:"Incorrect login or password"}
                );
            }
            res.status(200).send(
                {token: item}
            );
        }catch (e) {
            console.log('login', e);
            //return this.__sendFailedResponse(req, res, e);
        }
    }

    /**
     * Send link to the user email
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     */
    async requestRegistration(req, res)
    {
		const mappedObject = new RegistrationApiMapper().toServerProperty(req.body);


        const usersService = serviceContainer.getService('internal', 'users');
        const result = await usersService.getUserByEmail(mappedObject.email);

        if(result && result.uid)
        {
            return res.status(400).send(
                {code: "login.0.4", message:"User with this email already exist"}
            );
        }


        try{
            const token = await new LoginService().generateToken(mappedObject);
            const createdUser = await usersService.createUser(mappedObject);

            res.status(200).send(
                { userData: result, token: item || null}
            );
        }catch (e) {
            console.log('requestRegistration', e);
            return res.status(500).send(
                {code: "login.0.2", message:"Something wrong with notificationService"}
            );
        }
    }
}

module.exports = Api;
