//this wrapper should incoupsulate all business logic
const pasport = require("passport");
const NotFoundEntityException = require("./exceptions/NotFoundEntityException");
const InternalErrorException = require("./exceptions/InternalError");
const jwt = require("jsonwebtoken");
const conf = require("../../config/server");

const LocalStrategy = require("./strategies/Local");

class LoginService
{
    login(method, request) {

        return new Promise( (resolve, reject) => {
            let strategy = this.__getStrategyByMethod(method);
            if(!strategy)
            {
                return reject("No strategies found");
            }
            pasport.use(method, strategy);

            return pasport.authenticate(method, {}, (err, userData) => {

                if(err) return reject(err);
                if(!userData) return resolve(null);
                return resolve(this.generateToken(userData))

            })(request);
        });
    }

    __getStrategyByMethod(method)
    {
        switch(method)
        {
            case 'local':
                return new LocalStrategy();
        }
    }

    generateToken(userData)
    {
        console.log('generateToken', userData);
        return jwt.sign(userData, conf.jwt_secret_key, {
            expiresIn: conf.jwt_expires_in
        });
    }

    getDataFromToken(token)
    {
        return new Promise((resolve, reject) => {
            jwt.verify(token, conf.jwt_secret_key, (err, decoded) => {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        return reject("Token expired")
                    }
                    return reject();
                }
                resolve(decoded);
            });
        });
    }
}

module.exports = LoginService;
