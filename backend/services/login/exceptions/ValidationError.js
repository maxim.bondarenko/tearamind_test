const InternalError = require("./InternalError");

class ValidationError extends InternalError
{

}

module.exports = ValidationError;