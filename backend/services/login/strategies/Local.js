const passportStr = require('passport-strategy');
const serviceContainer = require("../../../libs/ServiceContainer");


class Local extends passportStr
{
    authenticate(req, option)
    {
        if(!req.email || !req.password)
        {
            this.error("Mandatory fields is absents");
        }
        const usersService = serviceContainer.getService('internal', 'users');
        usersService.getByUsernamePassword(req.email, req.password).then((userData) => {
            this.success(userData);
        }).catch(
            err => {
                this.fail('User credential not valid');
            }
        );
    }
}

module.exports = Local;
