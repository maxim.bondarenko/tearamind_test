const oauthService = require("../OauthService");
const { secret_key_header_name, secret_key_communication } = require('../../../config/server');


class InternalApi
{
    async decryptData(token)
    {
        return new oauthService().decryptTokenData(token);
    }

    async addUserDataToRequest(req, res, next)
    {
        const authHeader = req.headers['x-access-token'];
        const authInternalHeader = req.headers[secret_key_header_name];
        if(authInternalHeader && authInternalHeader === secret_key_communication)
        {
            req.user = {uid:-1};
            return next();
        }

        if(authHeader)
        {
            try{
                req.user = await new oauthService().decryptTokenData(authHeader);
            }catch(e)
            {
                console.log(e);
                return res.status(401).send(
                    { message: "Not authorize" }
                );
            }
        }
        next();
    }
}

module.exports = InternalApi;