const jwt = require("jsonwebtoken");
const tokenConf = require("./config/token");

class Token
{
    decryptTokenData(token)
    {
        return new Promise((resolve, reject) => {
            jwt.verify(token, tokenConf.jwt_secret_key, (err, decoded) => {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        reject("Token expired")
                    }
                    return reject(err);
                }
                resolve(decoded);
            });
        });
    }
}
module.exports = Token;