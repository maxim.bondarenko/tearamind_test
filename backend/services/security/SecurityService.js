const service_api_map = require("./config/service_api_map");
const UserNotAuthorizeError = require("./exceptions/UserNotAuthorizeError");
const errorsCodes = require("./exceptions/ExceptionsCodes");

class SecurityService
{
    /**
     * Apply all restictions for public api
     *
     * @param {Object} req  request object from express nodejs module.
     */
    async processApiPermissions(req)
    {
        let routeUrl = this.__prepareKey(req.method, req.route.path);
        const matchUrlData = service_api_map[routeUrl] || service_api_map[req.method.toLowerCase()+'_*'];
        if(matchUrlData)
        {
            if(this.checkIsUserNeedAuthorized(matchUrlData) && (!req.user || !req.user.role))
            {
                throw new UserNotAuthorizeError(errorsCodes.OAUTH.NOT_AUTHORIZE);
            }
            return this.runConditions(matchUrlData, req);
        }
    }

    checkIsUserNeedAuthorized(apiRestrictions)
    {
        return (apiRestrictions.shouldAuthentificate && apiRestrictions.shouldAuthentificate === true);
    }

    runConditions(apiRestrictions, req)
    {
        if(typeof apiRestrictions.conditionFunction === 'function')
        {
            return apiRestrictions.conditionFunction(req);
        }
    }

    __prepareKey(method, routePath)
    {
        return method.toLowerCase()+'_'+routePath;
    }
}

module.exports = new SecurityService();