
module.exports = {
    "get_*": {service: "testsService", shouldAuthentificate: true},
    "post_*": {service: "testsService", shouldAuthentificate: true, permission: 'rw'}
};