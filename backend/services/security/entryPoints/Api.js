const async = require("async");
const securityService = require("../SecurityService");
const exceptionMapper = require("../mappers/ExceptionApiStatusMapper");

class Api
{
    /**
     * It's middleware method that apply all restriction and return error if there is
     *
     * @param {Object} res  response object from express nodejs module
     * @param {Object} req  request object from express nodejs module.
     * @param {Function} next  use for contiue resolveing route.
     */
    async processRequest(req, res, next)
    {
        try {
            await securityService.processApiPermissions(req);
            next();
        }
        catch(exception) {
            res.status(exceptionMapper.getApiStatus(exception)).send(
                exception
            );
            console.log(exception, exception.stack ? exception.stack.split("\n") : '');
        }
    }
}

module.exports = Api;