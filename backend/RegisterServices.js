const serviceContainer = require("./libs/ServiceContainer");
//======================================================================================================================

//router
const router = require("./libs/Router");
serviceContainer.registerServiceEntryPoints('internal', 'router', router);
//======================================================================================================================

//files services

//======================================================================================================================

//users services
const usersApi = require("./services/users/entryPoints/InternalApi");

serviceContainer.registerServiceEntryPoints('internal', 'users', usersApi);
//======================================================================================================================

//security service
let oauthInternal = require("./services/oauth/entryPoints/InternalApi");
router.setMiddleWareFunction(new oauthInternal().addUserDataToRequest);

let securityService = require("./services/security/entryPoints/Api");
router.setMiddleWareFunction(new securityService().processRequest);
//======================================================================================================================