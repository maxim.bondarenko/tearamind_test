const express = require('express');
const bodyParser = require('body-parser');
const mongooseLib = require("./libs/mongoose");
const dbConf = require('./config/db');
//run service registration;
require("./RegisterServices");

// Configuration
const { port } = require('./config/server');
const ApiSchema = require('./config/ApiSchema');

const ServiceContainer = require("./libs/ServiceContainer");

const apiRouter = ServiceContainer.getService('internal', 'router');



mongooseLib.connect(dbConf.db, dbConf.host, dbConf.dbName, dbConf.port)
    .catch((e) => { console.log("DB CONNECT ERROR: "); console.log(e); });

mongooseLib.setHandlerOnce('open', () => {
    console.log("DB open");
    new AppServer().run();
});

class AppServer
{
    constructor()
    {
        this.express = express();
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(bodyParser.json());
    }
    async run()
    {
        //register api
        this.setHeaders();
        this.setRoutes();
        this.express.listen(port, () => {
            console.log('Server started on port ' + port);
        });
    }

    setRoutes()
    {
        apiRouter.initMiddleWares(this.express);
        apiRouter.buildApiRoutes(ApiSchema);
        this.express.use('/api', apiRouter.getApiRoutes());
    }

    setHeaders()
    {
        this.express.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Access-Token, X-Access-Type, Content-Type, Accept");
            res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH");
            next();
        });
    }
}

