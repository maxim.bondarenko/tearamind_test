
const allowsApiMethods = ['get', 'put', 'post', 'patch', 'delete', 'head'];
const exp = require('express');

class Router
{
    constructor()
    {
        this.routes = {api: []};
        this.middleWares = [];
    }

    buildApiRoutes(schema)
    {
        let api = exp.Router();

        for(let method in schema)
        {
            if(!schema.hasOwnProperty(method))
            {
                continue;
            }

            if(allowsApiMethods.indexOf(method) === -1)
            {
                console.log(method+" in schema NOT allowed");
                continue;
            }

            if(typeof schema[method] !== 'object')
            {
                console.log(method+" should be object");
                continue;
            }

            for(let url in schema[method])
            {
                if(!schema[method].hasOwnProperty(url) || !schema[method][url] || typeof schema[method][url] !== 'function')
                {
                    continue;
                }
                if(this.middleWares && this.middleWares.length > 0)
                {
                    api[method](url, ...this.middleWares, schema[method][url]);
                }
                else{
                    api[method](url, schema[method][url]);
                }
            }
        }
        this.routes.api = api;
    }

    getApiRoutes()
    {
        return this.routes.api;
    }

    setMiddleWareFunction(middleWareFunction)
    {
        if(typeof middleWareFunction !== 'function')
        {
            throw new Error("MiddleWare should be function");
        }
        this.middleWares.push(middleWareFunction);
    }

    initMiddleWares(express)
    {
        console.log("start setting middleWares");
        for(let i = 0; i < this.middleWares.length; i++)
        {
            express.use(this.middleWares[i]);
        }
        console.log("middleWares were set");
    }
}

module.exports = new Router();