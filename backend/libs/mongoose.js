const mongoose = require('mongoose');
//const autoIncrement = require('mongoose-auto-increment');
module.exports =
    {
        connect(db, host, dbName, port)
        {
            mongoose.Promise = Promise;
            let strConn = db+'://'+host+'/'+dbName;
            if (port)
            {
                strConn =  db+'://'+host+':'+port+'/'+dbName;
            }
            return mongoose.connect(strConn,
                {
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                });
            //autoIncrement.initialize(connection);
        },

        getAutoIncrement()
        {
            //return autoIncrement;
        },

        setHandler(eventName, callback)
        {
            mongoose.connection.on(eventName, function(...data){
                console.log(eventName, ...data);
                callback(...data);
            });
        },

        setHandlerOnce(eventName, callback)
        {
            mongoose.connection.once(eventName, function(...data){
                console.log(eventName, ...data);
                callback(...data);
            });
        },

        mongoose: mongoose
    };
