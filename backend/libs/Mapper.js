const ValidationError = require("./ValidationError");

class Mapper
{
    toClientProperty(dataObject, mappingProperties)
    {
        if(!dataObject)
        {
            return dataObject;
        }
        let mappedObject = {};
        let props = mappingProperties || this.getPropertyMapping();
        for(let i = 0; i < props.length; i++)
        {
            if(props[i].toClienSkipped)
            {
                continue;
            }
            if(props[i].toServer && props[i].toClient && dataObject.hasOwnProperty(props[i].toServer))
            {
                mappedObject[props[i].toClient] = props[i].toClientFunc ?
                    props[i].toClientFunc(dataObject[props[i].toServer], dataObject) : dataObject[props[i].toServer];
            }
        }

        return mappedObject;
    }

    toServerProperty(dataObject, mappingProperties)
    {
        let mappedObject = {};
        let props = mappingProperties || this.getPropertyMapping();
        for(let i = 0; i < props.length; i++)
        {
            if(props[i].toServerSkipped)
            {
                continue;
            }
            if(props[i] && props[i].toClient && dataObject.hasOwnProperty(props[i].toClient))
            {
                if(props[i].type && this.__getobjectType(dataObject[props[i].toClient]) !== props[i].type)
                {
                    throw new ValidationError("Incorrect type "+props[i].toClient+". Should be "+props[i].type)
                }
                if(props[i].validateFunction)
                {
                    props[i].validateFunction(dataObject[props[i].toClient], dataObject);
                }
                mappedObject[props[i].toServer] = props[i].toServerFunc ?
                    props[i].toServerFunc(dataObject[props[i].toClient], dataObject) : dataObject[props[i].toClient];
            }
            else if(props[i].required && props[i].required === true)
            {
                throw new ValidationError("Field "+props[i].toClient+" is required");
            }
        }

        return mappedObject;
    }

    getPropertyMapping()
    {
        throw new ValidationError("Method getToClientProperty should be declare in child class");
    }

    __getobjectType(obj)
    {
        if(Array.isArray(obj))
        {
            return 'array';
        }
        return typeof obj;
    }

    getSearchFieldsMapping(languageLabel = null)
    {
        return [];
    }
}

module.exports = Mapper;