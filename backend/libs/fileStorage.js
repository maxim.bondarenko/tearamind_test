const multer = require("multer");
const mkdirp = require('mkdirp');
const path = require('path');
const mime = require('mime-types');

class FileStorage
{
    constructor(dirName = '', fileNameAutoGen = false, fileNameFunc)
    {
        try {
            // Configuring appropriate storage
            this.storage = multer.diskStorage({
                // Absolute path
                destination: function (req, file, callback) {
                    let dir = './uploads/'+dirName;
                    mkdirp(dir, err => callback(null, dir));
                },
                filename:  (req, file, callback) => {
                    console.log(this.__getFileName(file, fileNameFunc));
                    let fileName = fileNameAutoGen === true ? this.__getFileName(file, fileNameFunc) : file.fieldname || file.originalname;
                    callback(null, fileName);
                }
            });
        } catch (ex) {
            console.log("Error :\n"+ex);
        }
    }

    getUpload(fileFilter)
    {
        let data = { storage: this.storage};
        if(fileFilter && typeof fileFilter === 'function')
        {
            data.fileFilter = fileFilter;
        }
        return multer(data);
    }

    __getFileName(file, fileNameFunc)
    {
        if(typeof fileNameFunc === 'function')
        {
            return fileNameFunc(file);
        }
        return new Date().getTime().toString()
            + Math.floor(Math.random()*1000000)
            +this.__getFileExtention(file);
    }


    __getFileExtention(file)
    {
        let extension = path.extname( file.originalname || file.fieldname );
        if (!extension)
        {
            if(file.mimetype)
            {
                extension = '.'+mime.extension(file.mimetype);
            }
        }
        return extension;
    }
}

module.exports = FileStorage;