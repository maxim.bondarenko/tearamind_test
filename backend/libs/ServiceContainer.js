//it will help to move it to MSOA

const INTERNAL = 'internal';
const API = 'api';
const EVENTS = 'events';

class ServiceContainer
{

    constructor()
    {
        this.servicesEntyPoints = {
            'internal': {}, 'api': {}, 'events':{}, 'cli': {}
        };
    }

    get INTERNAL(){ return INTERNAL}
    get API(){ return API}
    get EVENTS(){ return EVENTS}

    get serviceEntryPointsTypes()
    {
        return [this.INTERNAL, this.API, this.EVENTS]
    }

    registerServiceEntryPoints(type, name, entryPoint) {
        if (this.serviceEntryPointsTypes.indexOf(type) === -1) {
            throw new Error(type + " it's not valid entry point type");
        }

        if (this.servicesEntyPoints[type][name]) {
            throw new Error(name + " impossible register service point twice");
        }
        this.servicesEntyPoints[type][name] = entryPoint;

    }

    getService(type, name)
    {
        return this.servicesEntyPoints[type] && this.servicesEntyPoints[type][name] ?
            this.servicesEntyPoints[type][name] : null;
    }

    getServicesByType(type)
    {
        return this.servicesEntyPoints[type] || {};
    }

    replaceServiceEntryPoints(type, name, entryPoint)
    {
        this.servicesEntyPoints[type][name] = entryPoint;
    }
}

//should be one instance
module.exports = new ServiceContainer();