const StatisticService = require("../services/statistic/entryPoints/Api");


const __callMethod = (req, res, serviceMethod) => {
    try{
         serviceMethod(req,res);
    }
    catch(exception)
    {
        console.log(exception.message);
        return res.status(500).send({ message: "something went wrong" })
    }
};

module.exports = {
    "get": {
        "/my/statistic": (req, res) => { __callMethod(req, res, new StatisticService().getMyStatistic)}
    },
};