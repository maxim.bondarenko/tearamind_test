const webpack = require("webpack");
const path = require("path");
const environment = process.env.NODE_ENV || 'development',

VENDOR_LIBS = [
	'@babel/polyfill',
	'react',
	'react-dom',
	'redux',
	'react-redux',
	'react-router-dom'
];

const config = {
	name: 'js',
	entry: {
		vendor: VENDOR_LIBS,
		app: './src/index.js'
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ["babel-loader"],
			},
		],
	},
	resolve: {
		extensions: ["*", ".js", ".jsx"],
	},
	output: {
		path: path.join(__dirname, 'public', 'build'),
		filename: 'build.[name].js',
		chunkFilename: 'build.[name].chunk.js',
		publicPath: '/'
	},
	mode: environment,
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			'process.env': JSON.stringify(process.env),

		})
	],
	devServer: {
		contentBase: path.resolve(__dirname, "./dist"),
		hot: true,
	},
};

module.exports = config;