import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
	oneOfType,
	object
} from 'prop-types';


// Actions
import * as authorization from '../store/actions/oauth.action.js';

// Components
import Logo from '../components/logo.jsx';
import ToggleForm from '../components/toggle-form.jsx';
import SignIn from '../components/signin.jsx';
import SignUp from '../components/signup.jsx';
import Warnings from '../../../library/warnings/index.jsx';

// rules
import rules from '../../../config/rules.json';

function mapStateToProps(state) {
	return { 
		...state.oauth,
		i18n: state.i18n,
		warnings: state.warnings
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators({
			...authorization,
			...languageActions
		}, dispatch),
		dispatch
	};
}

@connect(mapStateToProps, mapDispatchToProps)
class Login extends React.Component {
	static propTypes = {
		router: oneOfType([object]),
		match: oneOfType([object]).isRequired,
		actions: oneOfType([object]),
		warnings: oneOfType([object]),
		i18n: oneOfType([object])
	}

	static defaultProps = {
		router: {},
		actions: {},
		warnings: null,
		i18n: {}
	}

	constructor(props, context) {
		super(props, context);
		this.state = {
			textAnimation: false,
			animLeft: false,
			animRight: true
		};
		this.startAnimation = this.startAnimation.bind(this);
		this.left = this.left.bind(this);
		this.right = this.right.bind(this);
	}

	componentDidMount() {
		const { 
			match: { 
				params: { 
					token 
				} 
			},
			actions: {
				approveRegistration
			}
		} = this.props;

		if (token) {
			approveRegistration(token);
		}
	}
	
	__onSubmitSignUp = (value) => {
		const { actions: { registration } } = this.props;
		registration({ ...value });
	}

	__onSubmit = (value) => {
		const { actions: { login } } = this.props;
		login({ ...value });
	}
	
	left() {
		const { textAnimation } = this.state;
		if (!textAnimation) {
			this.setState({
				animLeft: false
			});
		} else {
			this.setState({
				animLeft: true
			});
		}
	}
	
	right() {
		const { textAnimation } = this.state;
		if (textAnimation) {
			this.setState({
				animRight: false
			});
		} else {
			this.setState({
				animRight: true
			});
		}
	}

	startAnimation() {
		const { textAnimation } = this.state;
		this.setState({
			textAnimation: !textAnimation
		});
	}

	changeLanguage(lang) {
		const {
			actions: {
				setTranslation
			}
		} = this.props;
		setTranslation(lang);
	}

	render() {
		const { textAnimation, animLeft, animRight } = this.state,
			{ 
				warnings,
				i18n: {
					language = ''
				}
			} = this.props,
			{ availableLanguages } = rules;

		return (
				<section className="authorization">
					<Warnings warnings={warnings} {...this.props} />
					<Logo textAnimation={textAnimation} />

					<CSSTransition
						in={textAnimation}
						appear={false}
						timeout={1500}
						classNames={{
							enter: 'languages--enter',
							enterActive: 'languages--enter-active',
							enterDone: 'languages--enter-done',
							exit: 'languages--exit',
							exitActive: 'languages--exit-active',
							exitDone: 'languages--exit-done'
						}}
					>
						<ul className="languages text absolute--core fl fl--align-c font--upper font--bold">
							{
								availableLanguages.map((item, i) => {
									return (
										<li className="languages__item" key={uniqid(i)}>
											{
												item === language ?
													(
														<span className="languages__link languages__link--active relative--core">{item}</span>
													)
													:
													(
														<span
															className="languages__link cursor--pointer relative--core"
															onClick={() => { this.changeLanguage(item); }}
															role="presentation"
														>
															{item}
														</span>
													)
											}
										</li>
									);
								})
							}
						</ul>
					</CSSTransition>

					<CSSTransition
					  in={textAnimation}
					  appear={false}
					  timeout={1500}
					  classNames={{
						enter: 'authorization__left--enter',
						enterActive: 'authorization__left--enter-active',
						enterDone: 'authorization__left--enter-done',
						exit: 'authorization__left--exit',
						exitActive: 'authorization__left--exit-active',
						exitDone: 'authorization__left--exit-done'
					  }}
					>
						<div className="authorization__left" />
					</CSSTransition>
					<div className="authorization__content">
						<CSSTransition
						  in={textAnimation}
						  appear={false}
						  timeout={500}
						  classNames={{
							enter: 'authorization__form-in--enter',
							enterActive: 'authorization__form-in--enter-active',
							enterDone: 'authorization__form-in--enter-done',
							exit: 'authorization__form-in--exit',
							exitActive: 'authorization__form-in--exit-active',
							exitDone: 'authorization__form-in--exit-done'
						  }}
						>
							<SignIn
							  onSubmit={this.__onSubmit}
							  {...this.props}
							/>
						</CSSTransition>
						<CSSTransition
						  in={textAnimation}
						  appear={false}
						  timeout={500}
						  classNames={{
							enter: 'authorization__form-up--enter',
							enterActive: 'authorization__form-up--enter-active',
							enterDone: 'authorization__form-up--enter-done',
							exit: 'authorization__form-up--exit',
							exitActive: 'authorization__form-up--exit-active',
							exitDone: 'authorization__form-up--exit-done'
						  }}
						>
							<SignUp
							  initialValues={{ terms: true }}
							  onSubmit={this.__onSubmitSignUp}
							  {...this.props}
							/>
						</CSSTransition>
					</div>
					<CSSTransition
					  in={textAnimation}
					  appear={false}
					  timeout={1000}
					  classNames={{
						enter: 'authorization__toggle--enter',
						enterActive: 'authorization__toggle--enter-active',
						enterDone: 'authorization__toggle--enter-done',
						exit: 'authorization__toggle--exit',
						exitActive: 'authorization__toggle--exit-active',
						exitDone: 'authorization__toggle--exit-done'
					  }}
					>
						<ToggleForm
						  textAnimation={textAnimation}
						  animLeft={animLeft}
						  animRight={animRight}
						  left={this.left}
						  right={this.right}
						  startAnimation={this.startAnimation}
						  {...this.props}
						/>
					</CSSTransition>
					<CSSTransition
					  in={textAnimation}
					  appear={false}
					  timeout={1500}
					  classNames={{
						enter: 'authorization__right--enter',
						enterActive: 'authorization__right--enter-active',
						enterDone: 'authorization__right--enter-done',
						exit: 'authorization__right--exit',
						exitActive: 'authorization__right--exit-active',
						exitDone: 'authorization__right--exit-done'
					  }}
					>
						<div className="authorization__right" />
					</CSSTransition>
				</section>
		);
	}
}

export default Login;
