import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Actions
import * as groupAction from '../actions/group.action.js';

// Components


function mapStateToProps(state) {
	return {
		...state
	};
}

function mapDispatchToProps(dispatch) {
	return {
		// actions: bindActionCreators({
		// 	...groupAction
		// }, dispatch),
		// dispatch
	};
}

@connect(mapStateToProps, mapDispatchToProps)
class Test extends React.Component {

	render() {
		return (
			<React.Fragment>
				My test
			</React.Fragment>
		);
	}
}

export default Group;
