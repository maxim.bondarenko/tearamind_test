import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers/test.rd.js';

const enhancer = compose(
    applyMiddleware(thunk)
);

export default function configureStore(initialState = {}) {
    const store = createStore(reducers, initialState);
    return store;
}
