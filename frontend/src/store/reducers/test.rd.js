
const initialState = {};

export default function group(state = initialState, action) {
	switch (action.type) {
		default:
			return { ...state };
	}
}
