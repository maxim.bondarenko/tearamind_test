import jwtDecode from 'jwt-decode';
import {
	LOG_IN,
	LOG_OUT,
	SET_FETCH
} from '../constants/oauth.const.js';
import Api from '../../../service/api.js';
import { SET_WARNINGS } from '../../../library/warnings/constant/warnings.const.js';

export function login(data) {
	return async (dispatch) => {
		try {
			dispatch({
				type: SET_FETCH,
				fetch: {
					signin: true
				}
			});

			const autentification = await Api.__request({
				url: 'login/local',
				method: 'POST',
				data,
				type: 'login'
			});

			if (autentification) {
				localStorage.authToken = autentification.token;

				dispatch({
					type: LOG_IN,
					fetch: {
						signin: false
					},
					user: jwtDecode(autentification.token)
				});
			}
		} catch (err) {
			dispatch({
				type: SET_FETCH,
				fetch: {
					signin: false
				}
			});
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}

export function registration(data) {
	return async (dispatch) => {
		try {
			dispatch({
				type: SET_FETCH,
				fetch: {
					signup: true
				}
			});

			data.service = 'survey';
			await Api.__request({
				url: 'request/registration',
				method: 'POST',
				data,
				type: 'login'
			});

			dispatch({
				type: SET_FETCH,
				fetch: {
					signup: false
				}
			});

			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'success',
					message: `Activation letter was sent to your email adress ${data.email}`
				}
			});
		} catch (err) {
			dispatch({
				type: SET_FETCH,
				fetch: {
					signup: false
				}
			});
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}

export function approveRegistration(token) {
	return async (dispatch) => {
		try {
			const { userData, token: bToken } = await Api.__request({
				url: `approve/registration/${token}`,
				method: 'POST',
				type: 'login'
			});

			if (userData && bToken) {
				localStorage.authToken = bToken;

				dispatch({
					type: LOG_IN,
					user: userData
				});
			} else {
				dispatch({
					type: SET_WARNINGS,
					warnings: {
						type: 'error',
						message: 'User was not received. Try to signup again!'
					}
				});
			}
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}

export function logout() {
	return (dispatch) => {
		dispatch({
			type: LOG_OUT
		});
	};
}

export function sendRestoreEmail(data) {
	return async (dispatch) => {
		try {
			data.service = 'survey';

			const response = await Api.__request({
				url: 'request/reset_password',
				method: 'POST',
				data,
				type: 'login'
			});

			if (response) {
				dispatch({
					type: SET_WARNINGS,
					warnings: {
						type: 'success',
						message: `Restore link was sent to ${data.email}, follow it please!`
					}
				});
			} else {
				dispatch({
					type: SET_WARNINGS,
					warnings: {
						type: 'error',
						message: 'Something went wrong, try again please!'
					}
				});
			}
		} catch (err) {
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}

export function approveResetPassword(token, newPass) {
	return async (dispatch) => {
		try {
			dispatch({
				type: SET_FETCH,
				fetch: {
					restore: true
				}
			});

			const response = await Api.__request({
				url: `approve/reset_password/${token}`,
				method: 'POST',
				type: 'login',
				data: { newPass }
			});

			if (response) {
				dispatch({
					type: SET_FETCH,
					fetch: {
						restore: false
					}
				});
				dispatch({
					type: SET_WARNINGS,
					warnings: {
						type: 'success',
						message: 'Your password was changed successfully!'
					}
				});
			} else {
				dispatch({
					type: SET_WARNINGS,
					warnings: {
						type: 'error',
						message: 'Something went wrong, try again please!'
					}
				});
			}
		} catch (err) {
			dispatch({
				type: SET_FETCH,
				fetch: {
					restore: false
				}
			});
			dispatch({
				type: SET_WARNINGS,
				warnings: {
					type: 'error',
					message: err instanceof Object && err.text ? err.text : err,
					code: err instanceof Object && err.code ? err.code : null
				}
			});
		}
	};
}
