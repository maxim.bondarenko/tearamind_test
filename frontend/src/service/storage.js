const storage = (() => {
    const methods = (webstorage) => {
        return {
            __set(value, name) {
                window[webstorage].setItem(name, value);
            },

            __setJSON(value, name) {
                window[webstorage].setItem(name, JSON.stringify(value));
            },

            __setInJSON(value, nameOfObject, propsOfReplace) {
				let json = window[webstorage].getItem(nameOfObject);
                if (json && json.isJSON()) {
                    json = JSON.parse(json);
                    if (propsOfReplace) {
                        window[webstorage].setItem(nameOfObject, JSON.stringify({
                            ...json,
                            [propsOfReplace]: value
                        }));
                    } else if (value instanceof Object) {
                        window[webstorage].setItem(nameOfObject, JSON.stringify({
                            ...json,
                            ...value
                        }));
                    }
                } else if (propsOfReplace) {
					window[webstorage].setItem(nameOfObject, JSON.stringify({
						[propsOfReplace]: value
					}));
                } else {
					window[webstorage].setItem(nameOfObject, JSON.stringify(value));
				}
            },

            __get(name) {
                return window[webstorage].getItem(name);
            },

            __getJSON(name) {
                const json = window[webstorage].getItem(name);
                if (json && json.isJSON()) {
                    return JSON.parse(json);
                }
                return null;
            },

            __clear(name) {
                window[webstorage].removeItem(name);
            },

            __clearAll() {
                window[webstorage].clear();
            }
        };
    };

    return {
        session: methods('sessionStorage'),
        local: methods('localStorage')
    };
})();

export default storage;
