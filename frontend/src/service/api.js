import configuration from '../config/host.js';

const {
	origin,
} = configuration,

API_URL = `${origin}`,
API_URL_LOGIN_PORTAL = `${origin}`,
API_FILE_STORAGE = `${origin}`;

class Api {
	static __request({
		 url,
		 method,
		 data,
		 headers,
		 type
	 }) {
		const conf = {};
		let path = '';

		switch (method) {
			case 'GET':
				conf.method = method;
				conf.headers = {
					'X-Access-Token': localStorage.getItem('authToken')
				};
				break;
			case 'POST':
			case 'PATCH':
			case 'PUT':
			case 'DELETE':
				conf.body = JSON.stringify(data);
				conf.headers = {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'X-Access-Token': localStorage.getItem('authToken'),
					...headers
				};
				conf.method = method;
				break;
			case 'FILE':
				conf.body = data;
				conf.headers = {
					'X-Access-Token': localStorage.getItem('authToken'),
					...headers
				};
				conf.method = 'POST';
				break;
			default:
				break;
		}

		switch (type) {
			case 'login':
				path = API_URL_LOGIN_PORTAL;
				break;
			case 'results': 
				path = API_URL_RESULTS;
				break;
			case 'users': 
				path = API_URL_USERS;
				break;
			case 'billing': 
				path = API_URL_BILLING;
				break;
			case 'files': 
				path = API_FILE_STORAGE;
				break;
			default: 
				path = API_URL;
		}

		return fetch(`${path}/api/${url}`, conf).then((res) => {
				 const contentType = res.headers.get('content-type');

				 if (res.status === 401) {
					location.href = '/logout';
				 } else if (res.status >= 400 && res.status <= 526) {
					 const err = { status: res.status, error: res.json() };
					 throw err;
				 } else if (res.status === 204) {
					return true;
				 } else if (contentType && contentType.indexOf('application/json') !== -1) {
					return res.json();
				 } else {
					return res.text();
				 }

				 return false;
			 })
			 .then((response) => (response))
			 .catch((e) => (
				 (async () => {
					 if (e instanceof Object && e.error instanceof Promise) {
						const { error } = e,
							   { message, code } = await error;

						let msg = '';

						if (typeof message === 'string') {
							msg = message;
						} else if (typeof message === 'object') {
							msg = message.message || message.stack;
						}

						const text = `${msg}`;
						
						throw { text, code };
					 } else {
						 throw e;
					 }
				 })()
			 ));
	}
}

export default Api;
