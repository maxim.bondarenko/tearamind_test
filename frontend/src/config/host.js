const {
	NODE_ENV,
	BACKEND_HOST,
	BACKEND_PORT
} = process.env;

module.exports = {
	"serviceName": "backend",
	"protocol": NODE_ENV == 'production' ? 'https://' : 'http://',
	"host": BACKEND_HOST,
	"port": BACKEND_PORT,
	"origin": `${NODE_ENV == 'production' ? 'https://' : 'http://'}${BACKEND_HOST}${BACKEND_PORT ? `:${BACKEND_PORT}` : ''}`,
};

export default configuration;
