import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router
} from 'react-router-dom';
import Main from './main.jsx';
import configureStore from './store/store.js';

const store = configureStore({});
ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Main />
        </Router>
    </Provider>,
    document.getElementById('app'));
