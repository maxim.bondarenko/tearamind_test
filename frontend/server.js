const express = require('express'),
    app = express(),
    path = require('path'),
    history = require('connect-history-api-fallback');

if (process.env.NODE_ENV !== 'production') {
    console.warn(process.env.NODE_ENV ? process.env.NODE_ENV : 'NODE_ENV variable doesn`t definded');

    const webpack = require('webpack'),
        webpackDevMiddleware = require('webpack-dev-middleware'),
        webpackConfig = require('./webpack.config.js'),
        compiler = webpack(webpackConfig);

    app.use(express.static('public/'));

    app.use(history({
        disableDotRule: true,
        htmlAcceptHeaders: ['text/html', 'application/xhtml+xml']
    }));

    app.use(webpackDevMiddleware(compiler, {
        publicPath: webpackConfig.output.publicPath,
    }));
} else {
    console.warn(process.env.NODE_ENV);

    app.use(express.static('public'));

    app.use(history({
        disableDotRule: true,
        htmlAcceptHeaders: ['text/html', 'application/xhtml+xml']
    }));

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'public/index.html'));
    });
}

app.listen(process.env.FRONTEND_PORT, () => console.warn(process.env.FRONTEND_PORT));
